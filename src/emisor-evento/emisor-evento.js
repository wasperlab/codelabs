import {LitElement,html, css} from 'lit-element';

class EmisorEvento extends LitElement{
    static get properties(){
        return{
           
        };
    }

   
    constructor(){
        super();
      
    }

    render(){
        return html`
        <h2>Emisor evento</h2>
        <button @click="${this.sendEvent}"> No pulsar </button>
        `;

    }

    sendEvent(e){
        console.log("pulsado boton");
        console.log(e);

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail" : {
                        "course": "TechU",
                        "year": 2020
                    }
                }
            )
        )
    }
}

customElements.define('emisor-evento',EmisorEvento);