import {LitElement,html} from 'lit-element';




class FichaPersona extends LitElement{
    static get properties(){
        
        return{
            name:{type:String},
            yearsInCompany:{type:Number},
            personInfo:{type:String},
            photo:{type:Object}

        };
    }

    constructor(){
        super();
        this.name="Prueba nombre";
        this.yearsInCompany=0;
        this.calculaPersonInfo();
    }
    render(){
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="name" value="${this.name}" @input="${this.updateName}"></input>
                <br/>
                <label>Años en la compañia</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br/>
                <input type="text" value="${this.personInfo}" disabled></input>
            </div>
        `;

    }

    updated(changedProperties){
        console.log("updated");
        changedProperties.forEach((oldValue,propName) =>{
            console.log("Propiedad " +propName + " cambia valor, anterior era " + oldValue);
        }
        );

        if(changedProperties.has("yearsInCompany")){
            console.log("Se ha cambiado el campo yearincompany "+this.yearsInCompany);
            this.calculaPersonInfo(); 
        }
    
            
        
    }
   
    calculaPersonInfo(){
        console.log("calculaPersonInfo");
        if (this.yearInCompany >= 7){
            this.personInfo ="lead";
        }else if (this.yearsInCompany >= 5){
            this.personInfo="senior";
        }else if(this.yearInCompany >=3){
            this.personInfo="team";
        }else if (this.yearsInCompany <=0){
            this.personInfo="desconocido";
        }else{
            this.personInfo="junior";
        }
    }

    updateYearsInCompany(e){

        console.log("updateYearsInCompany");
        console.log("yearincompany vale ",this.yearsInCompany);
        this.yearsInCompany=e.target.value;
       
    }

    updateName(e){
        console.log("updateName");
        this.name=e.target.value;
    }
}

customElements.define('ficha-persona',FichaPersona)