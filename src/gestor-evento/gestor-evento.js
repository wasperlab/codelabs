import {LitElement,html, css} from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../persona-sidebar/persona-sidebar.js';

class GestorEvento extends LitElement{
    static get properties(){
        return{
           
        };
    }

   
    constructor(){
        super();
      
    }

    render(){
        return html`
        <h2>Gestor evento</h2>
        <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
        <receptor-evento id="receiver"><receptor-evento>
        `;

    }
    processEvent(e){
        console.log("processEvent");
        console.log(e);

        this.shadowRoot.getElementById("receiver").course=e.detail.course;
        this.shadowRoot.getElementById("receiver").year=e.detail.year;
    }
}

customElements.define('gestor-evento',GestorEvento);