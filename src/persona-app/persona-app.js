import {LitElement,html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';


class PersonaApp extends LitElement{
    static get properties(){
        return{
            people:{type: Array}
           

        };
    }

    constructor(){
        super();
        this.people=[];
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
            <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
            <persona-main @updated-people="${this.updatePeople}" class="col-10"></persona-main>
        </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.peopleStatusUpdated}"></persona-stats>

        `;

    }

    updated(changedProperties){
        console.log("updated en persona-app");
        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people=this.people;
        }

    }

    peopleStatusUpdated(e){
        console.log("peopleStatusUpdated");
        console.log(e.detail);
        this.shadowRoot.querySelector("persona-sidebar").peopleStats=e.detail.peopleStats;
    }

    newPerson(e){
        console.log("newPerson en persona-app");
        console.log(e);
        this.shadowRoot.querySelector("persona-main").showPersonForm=true;
    }

    updatePeople(e){
        console.log("updatePeople persona-app");
        this.people=e.detail.people;
    }
}

customElements.define('persona-app',PersonaApp);