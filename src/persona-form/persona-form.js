import {LitElement,html, css} from 'lit-element';

class PersonaForm extends LitElement{
    static get properties(){
        return{
            person: {type: Object},
            editingPerson:{type:Boolean}
        
        };
    }

   
    constructor(){
        super();
        this.resetFormData();
        this.editingPerson=false;
      
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" 
                    @input="${this.updateName}" 
                    .value="${this.person.name}" 
                    ?disabled="${this.editingPerson}"
                    class="form-control" 
                    placeholder="Nombre completo"/>
                </div>
                <div class="form-group">
                    <label>Perfil</label>
                    <textarea @input="${this.updateProfile}" .value="${this.person.profile}" type="text" class="form-control" placeholder="Perfil"></textarea>
                </div>
                <div class="form-group">
                    <label>Años en la empresa</label>
                    <input @input="${this.updateYearInCompany}" .value="${this.person.yearsInCompany}" "type="text" class="form-control" placeholder="Años"/>
                </div>
                <button @click="${this.goBack}" class="btn btn-primary">Atrás</button>
                <button @click="${this.storePerson}" class="btn btn-success">Guardar</button>
            </form>
        </div>
      
        `;

    }
    updateName(e){
        console.log("updateName persona-form");
        this.person.name=e.target.value;
    }
    updateProfile(e){
        console.log("updateProfile persona-form");
        this.person.profile=e.target.value;
    }

    updateYearInCompany(e){
        console.log("updateYearInCompany persona-form");
        this.person.yearsInCompany=e.target.value;
    }

    goBack(e){
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        
    }

    storePerson(e){
        console.log("storePerson");
        e.preventDefault();
        this.person.photo={
            "src":"./assets/persona-img.jpg",
            "alt" : "Persona"
        }

        console.log("La propiedad name en person vale "+ this.person.name);
        console.log("La propiedad name en person vale "+ this.person.profile);
        console.log("La propiedad name en person vale "+ this.person.yearsInCompany);

        
        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }));
        this.resetFormData();
        
    }

    resetFormData(){
        console.log("resetFormData");
        this.person={};
        this.person.name="";
        this.person.profile="";
        this.person.yearsInCompany="";
        this.editingPerson=false;
    }
    
}

customElements.define('persona-form',PersonaForm);