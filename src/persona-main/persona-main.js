import {LitElement,html, css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
class PersonaMain extends LitElement{
    static get properties(){
        return{
            people:{type:Array},
            showPersonForm:{type:Boolean}
        };
    }

    static get styles(){
        return css`
         :host{
             all: initial;


         }
        `;
    }

    constructor(){
        super();
        this.showPersonForm=false;
        this.people=[
            {
                name:"Roland",
                profile: "ccaca",
                yearsInCompany: 10,
                photo:{
                    src:"./assets/persona-img.jpg",
                    alt:"Roland"
                }
            },
            {
                name:"Pepa",
                profile: "ccaca",
                yearsInCompany: 1,
                photo:{
                    src:"./assets/persona-img.jpg",
                    alt:"Pepa"
                }
            },
            {
                name:"Paco",
                profile: "ccaca",
                yearsInCompany: 13,
                photo:{
                    src:"./assets/persona-img.jpg",
                    alt:"Paco"
                }
            },
            {
                name:"Juana",
                profile: "ccaca",
                yearsInCompany: 13,
                photo:{
                    src:"./assets/persona-img.jpg",
                    alt:"Juana"
                }
            }
            ,{
                name:"Bola",
                profile: "ccaca",
                yearsInCompany: 13,
                photo:{
                    src:"./assets/persona-img.jpg",
                    alt:"Bola"
                }
            }
        ]
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`<persona-ficha-listado
                                    fname="${person.name}"
                                    yearsInCompany="${person.yearsInCompany}"
                                    profile="${person.profile}"
                                    .photo="${person.photo}"
                                    @delete-person="${this.deletePerson}"
                                    @info-person="${this.infoPerson}"
                                    ></persona-ficha-listado>` 
                )}
                </div>
            </div>
            <div class="row">
                <persona-form 
                @persona-form-close="${this.personFormClose}"
                @persona-form-store="${this.personFormStore}"
                class="d-none border rounded border-primary" 
                id="personaForm">
                </persona-form>
            </div>
        `;

    }

    updated(changedProperties){
        console.log("updated");

        if (changedProperties.has("showPersonForm")){
            console.log("ShowpersonForm ha cambiado en persona-main");

            if(this.showPersonForm === true){
                this.showPersonFromData();

            }else{
                this.showPersonList();
            }

        }
        if (changedProperties.has("people")){
            console.log("people ha cambiado en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people",
            {
                "detail":{
                    people: this.people
                }
            }
            ));
        }


    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a guardar una persona");
        console.log("La propiedad name en person vale "+ e.detail.name);
        console.log("La propiedad name en person vale "+ e.detail.profile);
        console.log("La propiedad name en person vale "+ e.detail.yearInCompany);

       
        //Nueva entrada
        if (e.detail.editingPerson===false){
            console.log("Nueva persona");
            this.people = [...this.people,e.detail.person];
        //Actualizacion
        }else{
            console.log("editar persona");
             this.people=this.people.map(
                person => person.name === e.detail.person.name ? person =e.detail.person : person
            );

        }
        this.showPersonForm=false;

    }

    personFormClose(){
        console.log("personFormClose");
        this.showPersonForm=false;

    }

    showPersonFromData(){
        console.log("showPersonFromData");
        console.log("Mostrando formulario");
        this.shadowRoot.getElementById("personaForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        
    }
    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("personaForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borra la persona "+e.detail.name);
        this.people=this.people.filter(
            person => person.name != e.detail.name 
        );
    }

    infoPerson(e){
        console.log("infoPerson");
        console.log("Se ha pedido más información de la persona de nombre " + e.detail.name);
    
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
    
        console.log(chosenPerson);
    
        let personToShow = {};
        personToShow.name  = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.tearsInCompany = chosenPerson[0].yearsInCompany;
    
        this.shadowRoot.getElementById("personaForm").person = personToShow;
        this.shadowRoot.getElementById("personaForm").editingPerson = true;
        this.showPersonForm = true;
    
    }
}

customElements.define('persona-main',PersonaMain);