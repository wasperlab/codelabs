import {LitElement,html} from 'lit-element';




class PersonaStats extends LitElement{
    
    static get properties(){
        return{
            people:{type: Array}
        };
    }

    constructor(){
        super();
        this.people=[];
    }

    render(){
        return html`
        <div>persona stats</div>
        `;

    }

    updated(changedProperties){
        console.log("updated en persona-stats");

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propriedad people en persona-stats");

            let peopleStats=this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                detail:{
                    peopleStats: peopleStats
                }
            }
            ))
        }
    }

    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo");
        console.log(people);
        let peopleStats={};
        peopleStats.numberOfPeople=people.length;

        return peopleStats;

    }
}

customElements.define('persona-stats',PersonaStats)