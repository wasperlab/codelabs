import {LitElement,html, css} from 'lit-element';

class TestApi extends LitElement{
    static get properties(){
        return{
            movies:{type:Array}
     
           
        };
    }

   
    constructor(){
        super();
        this.movies=[];
        this.getMovieData();
      
    }

    render(){
        return html`
        <h2>Test Api</h2>
        ${this.movies.map(
            movie => html `<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
        )}
        `;

    }

    getMovieData(){
        console.log("getMovieData")
        console.log("Obteniendo datos de las películas")
        let xhr= new XMLHttpRequest();
        xhr.onload= () => {
            if (xhr.status === 200){
                console.log("200. Status OK");
                let APIResponse =JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.movies=APIResponse.results;
            }

        };
        xhr.open("GET","https://swapi.dev/api/films/");
        xhr.send();
        console.log("Fin de getMoviedata");
    }

    
}

customElements.define('test-api',TestApi);