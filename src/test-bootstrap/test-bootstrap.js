import {LitElement,html,css} from 'lit-element';

class TestBootstrap extends LitElement{
    static get properties(){
        return{

        };
    }

    static get styles(){
        return css`
        .redbg{
            background-color:red;
        }
        .greenbg{
            background-color:green;
        }
        .bluebg{
            background-color:blue;
        }
        `;
    }

    constructor(){
        super();
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <h3> Test bootstrap</h3>
        <div class="row">
            <div class="col-2 col-sm-6 redbg">col1</div>
            <div class="col-3 col-sm-1 greenbg">col2</div>
            <div class="col-4 col-sm-1 bluebg">col3</div>
        </div>
        `;

    }
}

customElements.define('test-bootstrap',TestBootstrap);